package model.data_structures;

/**
 * Clase que representa a un nodo gen�rico. <br>
 */
public class Node<E> {

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Objeto que se va a guardar.
	 */
	private E data;

	/**
	 * Siguiente nodo en la lista encadenada.
	 */
	private Node<E> siguiente;


	// -----------------------------------------------------------------
	// Constructor
	// -----------------------------------------------------------------

	/**
	 * Crea un nuevo nodo gen�rico. <br>
	 * <b> post: </b> El el contenido del nodo se inicializó con los datos dados por parametro, su nodo siguiente es nulo.
	 * @param pData Objeto que se va a almacenar.
	 */
	public Node(E pData) {
		data = pData;
		siguiente = null;
	}

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Retorna el objeto almacenado.
	 * @return Objeto almacenado.
	 */
	public E darData() {
		return data;
	}

	/**
	 * Retorna el siguiente de la cadena.
	 * @return Nodo siguiente de la cadena.
	 */
	public Node<E> darSiguiente() {
		return siguiente;
	}

	/**
	 * Asigna al nodo siguiente el dado por parametro
	 */
	public void asignarSiguiente(Node<E> pSiguiente) {
		siguiente = pSiguiente;
	}
}