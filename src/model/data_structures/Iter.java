package model.data_structures;

import java.util.Iterator;

/**
 * Clase que representa el iterador para moverse a trav�s de los elementos guardados. <br>
 */
public class Iter<E> implements Iterator<E> {

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Nodo actual de la iteración.
	 */
	private Node<E> actual;

	// -----------------------------------------------------------------
	// Constructor
	// -----------------------------------------------------------------
	/**
	 * Crea un nuevo iterador. <br>
	 * <b> post: </b> El iterador se creó con su primer elemento siendo el principio de la lista
	 * @param pData principio, el primer nodo de la lista a iterar.
	 */
	public Iter(Node<E> principio) {
		actual = principio;
	}

	// -----------------------------------------------------------------
	// Métodos
	// -----------------------------------------------------------------
	/**
	 * Retorna si hay un siguiente elemento para recorrer.
	 * @return true si el nodo siguiente no es nulo, false de lo contrario.
	 */
	public boolean hasNext() {
		boolean rta = true;
		if (actual == null) {
			rta = false;
		}
		return rta;
	}

	/**
	 * Retorna el nodo siguiente de la iteración.
	 * @return Nodo siguiente de la iteración.
	 */
	public E next() {
		E data = actual.darData();
		actual = actual.darSiguiente();
		return data;
	}

}