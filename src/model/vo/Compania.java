package model.vo;

public class Compania 
{
	/**
	 * Nombre de la compa��a.
	 */
	private String nombre;
	
	/**
	 * N�mero de servicios que prest� la compa��a.
	 */
	private int numeroServicios;
	
	/**
	 * N�mero de taxis de una compa��a.
	 */
	private int numeroTaxis;
	
	/**
	 * Constrcutor.
	 * @param nombre
	 */
	public Compania(String nombre)
	{
		this.nombre = nombre;
		this.numeroServicios = 0;
		this.numeroTaxis = 0;
	}
	
	/**
	 * M�todo que retorna el nombre de la compa��a.
	 */
	public String nombre()
	{
		return nombre;
	}
	
	/**
	 * M�todo que retorna la cantidad de servicios realizados por los taxis de la compa��a.
	 */
	public int numeroServicios()
	{
		return numeroServicios;
	}
	
	/**
	 * M�todo que retorna la lista de taxis de la compa��a.
	 */
	public int numeroTaxis()
	{
		return numeroTaxis;
	}
	
	/**
	 * M�todo que agrega un servicio a la compa��a.
	 */
	public void agregarServicio()
	{
		numeroServicios++;
	}
	
	/**
	 * M�todo que agrega un taxi a la compa��a.
	 */
	public void agregarTaxi()
	{
		numeroTaxis++;
	}
	
	/**
	 * M�todo to string.
	 */
	public String toString()
	{
		return "Nombre: " + nombre + ". N�mero de taxis: " + numeroTaxis + ". N�mero de servicios: " + numeroServicios;
	}
	
}
