package model.vo;

import java.util.Iterator;

public class Taxi implements Comparable<Taxi>, Iterable<Taxi>
{
	/**
	 * Identificador del taxi.
	 */
	private String id;
	
	/**
	 * N�mero de servicios registrados por el taxi.
	 */
	private int numeroServicios;
	
	/**
	 * Total de distancia recorrida en los servicios del taxi.
	 */
	private double distanciaTotal;
	
	/**
	 * Total de ganancias generadas por los servicios del taxi.
	 */
	private double gananciaTotal;
	
	/**
	 * M�todo constructor del taxi
	 * @param id
	 */
	public Taxi(String id)
	{
		this.id = id;
		this.numeroServicios = 0;
		this.distanciaTotal = 0;
		this.gananciaTotal = 0;
	}
	
	/**
	 * M�todo que retorna el id del taxi.
	 */
	public String id()
	{
		return id;
	}
	
	/**
	 * M�todo que retorna el n�mero de servicios del taxi.
	 */
	public int numeroServicios()
	{
		return numeroServicios;
	}
	
	/**
	 * M�todo que retorna la distancia total de los servicios del taxi.
	 */
	public double distanciaTotal()
	{
		return distanciaTotal;
	}
	
	/**
	 * M�todo que retorna la ganancia total de los servicios del taxi.
	 */
	public double gananciaTotal()
	{
		return gananciaTotal;
	}
	
	/**
	 * M�todo que agrega un nuevo registro de servicio al taxi.
	 * @param distanciaServicio
	 * @param gananciaServicio
	 */
	public void agregarServicio(double distanciaServicio, double gananciaServicio)
	{
		this.numeroServicios++;
		this.distanciaTotal += distanciaServicio;
		this.gananciaTotal += gananciaServicio;
	}
	
	/**
	 * M�todo que transforma el taxi en un String.
	 */
	public String toString()
	{
		return "Taxi de id: " + id + ". Ha realizado una cantidad de servicios equivalente a: "
				+ numeroServicios + ". La distancia que ha recorrido en todos esos servicios es de: "
				+ distanciaTotal + ". Su ganancia total es de: " + gananciaTotal;
	}

	public int compareTo(Taxi o) {
		return this.id.compareTo(o.id);
	}

	@Override
	public Iterator<Taxi> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
}
