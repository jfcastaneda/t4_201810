package model.logic;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import model.data_structures.List;
import model.data_structures.MaxPQ;
import model.data_structures.MinPQ;
import model.utils.ComparatorCompaniaCantidadServicios;
import model.utils.ComparatorTaxiId;
import model.utils.ServicioAuxiliar;
import model.vo.Compania;
import model.vo.Taxi;

public class Chicago 
{	
	public static final SimpleDateFormat FORMAT = new SimpleDateFormat("YYYY-MM-DD'T'HH:mm:ss'.'000");

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";

	public static final ComparatorTaxiId COMPARATOR_TAXI_ID = new ComparatorTaxiId();

	public static final ComparatorCompaniaCantidadServicios COMPARATOR_COMPANIA_CANTIDAD_SERVICIOS
	= new ComparatorCompaniaCantidadServicios();

	/**
	 * MaxPQ para los taxis que empezaron en cierto rango de fechas.
	 */
	private MaxPQ<Compania> companiasMaxPQ;

	/**
	 * MinPQ para los taxis que empezaron en cierto rango de fechas.
	 */
	private MinPQ<Taxi> taxisMinPQ;

	/**
	 * M�todo constructor de la Ciudad.
	 */
	public Chicago()
	{
		// Oli
	}

	/**
	 * M�todo que carga la informaci�n de todos los taxis con servicios que ocurrieron en medio 
	 * de una fecha desde un archivo JSON.
	 * @param direccionJson
	 * @param fechaHoraInicial
	 * @param fechaHoraFinal
	 */
	public void cargarJson(String direccionJson, long fechaHoraInicial, long fechaHoraFinal) throws Exception
	{	
		ServicioAuxiliar servicio;
		Iterator<Taxi> iterTaxis;
		Iterator<Compania> iterCompanias;
		Taxi taxi = null;
		Compania compania = null;
		List<Taxi> taxis = new List<Taxi>();
		List<Compania> companias = new List<Compania>();
		boolean existe;
		InputStream inputStream = new FileInputStream(direccionJson);
		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
		Gson gson = new GsonBuilder().create();
		companiasMaxPQ = new MaxPQ<Compania>(COMPARATOR_COMPANIA_CANTIDAD_SERVICIOS);
		taxisMinPQ = new MinPQ<Taxi>(COMPARATOR_TAXI_ID);

		reader.beginArray();
		while(reader.hasNext())
		{
			servicio = gson.fromJson(reader, ServicioAuxiliar.class);
			if(servicio.inicio() >= fechaHoraInicial && servicio.fin() <= fechaHoraFinal)
			{
				existe = false;
				iterCompanias = companias.iterator();
				while(iterCompanias.hasNext())
				{
					compania = iterCompanias.next();
					if(compania.nombre().equals(servicio.company()))
					{			
						existe = true;
						break;
					}
				}
				if(!existe)
				{
					compania = new Compania(servicio.company());
					companias.agregarAlFinal(compania);
					companiasMaxPQ.insert(compania);
				}
				compania.agregarServicio();

				existe = false;
				iterTaxis = taxis.iterator();
				while(iterTaxis.hasNext())
				{
					taxi = iterTaxis.next();
					if(taxi.id().equals(servicio.taxiId()))
					{
						existe = true;
						break;
					}
				}
				if(!existe)
				{
					taxi = new Taxi(servicio.taxiId());
					taxis.agregarAlFinal(taxi);
					taxisMinPQ.insert(taxi);
					compania.agregarTaxi();
				}
				taxi.agregarServicio(servicio.distancia(), servicio.ganancia());
			}
		}
		System.out.println("TAXIS: ");
		while(!taxisMinPQ.isEmpty())
		{
			System.out.println(taxisMinPQ.delMin());
		}
		System.out.println("\nCOMPANIAS: ");
		while(!companiasMaxPQ.isEmpty())
		{
			System.out.println(companiasMaxPQ.delMax());
		}
		iterTaxis = taxis.iterator();
		while(iterTaxis.hasNext())
		{
			taxisMinPQ.insert(iterTaxis.next());
		}
		iterCompanias = companias.iterator();
		while(iterCompanias.hasNext())
		{
			companiasMaxPQ.insert(iterCompanias.next());
		}
	}

	/**
	 * M�todo que retorna el vaino sorteado :3
	 */
	public MinPQ<Taxi> taxisMinPQ()
	{
		return taxisMinPQ;
	}

	/**
	 * M�todo main. <br>
	 * S�, me dio pereza poner todo mejor, al final va a dar lo mismo solo implementar lo otro.
	 * @param args
	 */
	public static void main(String[] args) 
	{
		long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		long startTime = System.nanoTime();
		try
		{	
			Chicago chicago = new Chicago();
			chicago.cargarJson(DIRECCION_SMALL_JSON, 0, Long.MAX_VALUE);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		long endTime = System.nanoTime();
		long duration = (endTime - startTime)/(1000000);
		long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB\n");
		System.out.println("Hola, esto fue hecho con tiempo inicial 0 y tiempo final infinito, me ahorr� la parte de hacerles"
				+ " ver que ten�an que hundir un bot�n c: \nPd: No le hice un toString() a las Companias :3"
				+ "\nEdit: hecho el toString a las compa��as, ahora al parecer el algoritmo de Princeton se pife� :v");
	}
}
