package model.utils;

import java.text.ParseException;

import model.logic.Chicago;

public class ServicioAuxiliar 
{
	/**
	 * Fecha y hora de inicio del servicio.
	 */
	private String trip_start_timestamp = "-1";
	
	/**
	 * Fecha y hora de fin del servicio.
	 */
	private String trip_end_timestamp;
	
	/**
	 * Distancia recorrida por el servicio.
	 */
	private String trip_miles;
	
	/**
	 * Dinero pagado por el servicio.
	 */
	private String trip_total;
	
	/**
	 * Id del taxi que realiz� el servicio.
	 */
	private String taxi_id;
	
	/**
	 * Nombre de la compa��a del taxi que realiz� el servicio.
	 */
	private String company;
	
	/**
	 * M�todo constructor del servicio auxiliar.
	 * @param trip_start_timestamp
	 * @param trip_end_timestamp
	 * @param trip_miles
	 * @param trip_total
	 * @param taxi_id
	 * @param company
	 */
	public ServicioAuxiliar(String trip_start_timestamp, String trip_end_timestamp, 
			String trip_miles, String trip_total, String taxi_id, String company)
	{
		System.out.println(company);
		if(taxi_id != null)
		{
			this.taxi_id = taxi_id;
			this.trip_start_timestamp = trip_start_timestamp != null ? trip_start_timestamp : "0";
			this.trip_end_timestamp = trip_end_timestamp != null ? trip_end_timestamp : "" + Long.MAX_VALUE;
			this.trip_miles = trip_miles != null ? trip_miles : "0";
			this.trip_total	= trip_total != null ? trip_total : "0";
			this.company = company != null ? company : "Independent Owner";
			System.out.println(this.company);
		}
	}
	
	/**
	 * M�todo que retorna el tiempo de inicio del servicio.
	 * @throws ParseException si el formato est� mal. 
	 */
	public long inicio() throws ParseException 
	{
		return Chicago.FORMAT.parse(trip_start_timestamp).getTime();
	}
	
	/**
	 * M�todo que retorna el tiempo de fin del servicio.
	 * @throws ParseException si el formato est� mal.
	 */
	public long fin() throws ParseException
	{
		return Chicago.FORMAT.parse(trip_end_timestamp).getTime();
	}
	
	/**
	 * M�todo que retorna la distancia que recorri� el servicio.
	 */
	public double distancia()
	{
		return Double.parseDouble(trip_miles);
	}
	
	/**
	 * M�todo que retorna el costo del servicio.
	 */
	public double ganancia()
	{
		return Double.parseDouble(trip_total);
	}
	
	/**
	 * M�todo que retorna el id del taxi que realiz� el servicio.
	 */
	public String taxiId()
	{
		return taxi_id;
	}
	
	/**
	 * M�todo que retorna la compa��a del taxi que realiz� el servicio.
	 */
	public String company()
	{
		if(this.company == null)
		{
			this.company = "Independent Owner";
		}
		return company;
	}
	
	/**
	 * M�todo que convierte a String la clase.
	 */
	public String toString()
	{
		try
		{
			return "Este servicio fue realizado por el taxi con id: " + taxi_id 
					+ ". Inici� a la hora y fecha: " + trip_start_timestamp
					+ ", o en milisegundos: " + inicio()
					+ ". Termin� a la hora y fecha: " + trip_end_timestamp
					+ ", o en milisegundos: " + fin()
					+ ". Recorri� una distancia  de: " + trip_miles
					+ ". Su costo total fue: " + trip_total;
		}
		catch(ParseException pe)
		{
			return pe.getMessage();
		}
	}
}
