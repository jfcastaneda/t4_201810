package model.utils;

import java.util.Comparator;

import model.vo.Compania;

public class ComparatorCompaniaCantidadServicios implements Comparator<Compania>
{
	/**
	 * M�todo que compara dos compa��as por su cantidad de servicios
	 */
	public int compare(Compania arg0, Compania arg1) {
		int c1 = arg0.numeroServicios(), c2 = arg1.numeroServicios();
		if(c1 > c2)
		{
			return 1;
		}
		else if(c1 == c2){
			return 0;
		}
		else{
			return -1;
		}
	}

}
